## Simple Linux

This is a collection of BSD-like core utilities ported to Linux.
They are developed primarily for the Simple Linux project.

## Dependencies

In order to compile these tools the following is needed
`bash openssl clang make readline libmagic libacl cmake gcc bzlib-dev`

## Installing

These tools can be installed either as a replacement of GNU coreutils or as a compliment to them.
Therefore there are two installer scripts avaliable `convert` and `safereplace`.
`convert` is only meant to be used on "Alpine linux" to turn it into a "Simple Linux" install.
It is aimed primarily at advanced users and testers for now and is not to be used on mission-critical 
machines, as it may result in a lot of incomatibilities and breakages.

`safereplace` is a **safer** option and can be used on any linux distribution, hopefully, without 
issues. What this script does is simply alter your $PATH variabe to point to ~/.bsdbase, where these tools 
reside. This may cause some minor quirks here and there with cli software,but otherwise it is
safe to use. To use this option simply run the script and reboot.
This script enables you to use the tools from this repo while your software uses the GNU coreutils it came
with.

## Installing Simple Linux

Simple Linux is a Linux  distribution based on Alpine Linux and it attempts to bring the BSD experience to Linux
users. Right now it exists as a bash script meant to be run as root on top of an already existing 
Alpine Linux install.
`convert` script converts an Alpine linux installation into Simple Linux. What is does is replace some of 
the busybox tools with tools from this repository. For now...

Simple Linux is under development and the plans are:
1. Compile packages with clang as opposed to GCC.
2. Move to a BSD-like userland where things are more organised.
3. Introduce a 'compatibility layer' with glibc to make Alpine more usable as a daily driver
4. Provide ISO's with a GUI installer and a desktop environment aimed at less experienced users
5. Move to a different kernel, that is better optimized for desktop use

To install Simple Linux:
1. Install Alpine Linux
2. Install the alpine-sdk package
3. Clone this repo
4. Run `convert` script as root

Contributing
============
Contributions, critisism and ideas are always welcome and encouraged!
